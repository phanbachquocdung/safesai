// Collapse
!function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t(require("jquery")):"function"==typeof define&&define.amd?define(["jquery"],t):e.Util=t(e.jQuery)}(this,function(e){"use strict";return function(r){var t="transitionend";function e(e){var t=this,n=!1;return r(this).one(s.TRANSITION_END,function(){n=!0}),setTimeout(function(){n||s.triggerTransitionEnd(t)},e),this}var s={TRANSITION_END:"bsTransitionEnd",getUID:function(e){for(;e+=~~(1e6*Math.random()),document.getElementById(e););return e},getSelectorFromElement:function(e){var t=e.getAttribute("data-target");t&&"#"!==t||(t=e.getAttribute("href")||"");try{return document.querySelector(t)?t:null}catch(e){return null}},getTransitionDurationFromElement:function(e){if(!e)return 0;var t=r(e).css("transition-duration");return parseFloat(t)?(t=t.split(",")[0],1e3*parseFloat(t)):0},reflow:function(e){return e.offsetHeight},triggerTransitionEnd:function(e){r(e).trigger(t)},supportsTransitionEnd:function(){return Boolean(t)},isElement:function(e){return(e[0]||e).nodeType},typeCheckConfig:function(e,t,n){for(var r in n)if(Object.prototype.hasOwnProperty.call(n,r)){var i=n[r],a=t[r],l=a&&s.isElement(a)?"element":(o=a,{}.toString.call(o).match(/\s([a-z]+)/i)[1].toLowerCase());if(!new RegExp(i).test(l))throw new Error(e.toUpperCase()+': Option "'+r+'" provided type "'+l+'" but expected type "'+i+'".')}var o}};return r.fn.emulateTransitionEnd=e,r.event.special[s.TRANSITION_END]={bindType:t,delegateType:t,handle:function(e){if(r(e.target).is(this))return e.handleObj.handler.apply(this,arguments)}},s}(e=e&&e.hasOwnProperty("default")?e.default:e)}),function(e,t){"object"==typeof exports&&"undefined"!=typeof module?module.exports=t(require("jquery"),require("./util.js")):"function"==typeof define&&define.amd?define(["jquery","./util.js"],t):e.Collapse=t(e.jQuery,e.Util)}(this,function(e,s){"use strict";function i(e,t){for(var n=0;n<t.length;n++){var r=t[n];r.enumerable=r.enumerable||!1,r.configurable=!0,"value"in r&&(r.writable=!0),Object.defineProperty(e,r.key,r)}}function a(i){for(var e=1;e<arguments.length;e++){var a=null!=arguments[e]?arguments[e]:{},t=Object.keys(a);"function"==typeof Object.getOwnPropertySymbols&&(t=t.concat(Object.getOwnPropertySymbols(a).filter(function(e){return Object.getOwnPropertyDescriptor(a,e).enumerable}))),t.forEach(function(e){var t,n,r;t=i,r=a[n=e],n in t?Object.defineProperty(t,n,{value:r,enumerable:!0,configurable:!0,writable:!0}):t[n]=r})}return i}var u,l,c,t,n,g,f,h,d,m,p,_,y,v,C,T,o;return e=e&&e.hasOwnProperty("default")?e.default:e,s=s&&s.hasOwnProperty("default")?s.default:s,l="collapse",t="."+(c="bs.collapse"),n=(u=e).fn[l],g={toggle:!0,parent:""},f={toggle:"boolean",parent:"(string|element)"},h={SHOW:"show"+t,SHOWN:"shown"+t,HIDE:"hide"+t,HIDDEN:"hidden"+t,CLICK_DATA_API:"click"+t+".data-api"},d="show",m="collapse",p="collapsing",_="collapsed",y="width",v="height",C=".show, .collapsing",T='[data-toggle="collapse"]',o=function(){function o(t,e){this._isTransitioning=!1,this._element=t,this._config=this._getConfig(e),this._triggerArray=u.makeArray(document.querySelectorAll('[data-toggle="collapse"][href="#'+t.id+'"],[data-toggle="collapse"][data-target="#'+t.id+'"]'));for(var n=[].slice.call(document.querySelectorAll(T)),r=0,i=n.length;r<i;r++){var a=n[r],l=s.getSelectorFromElement(a),o=[].slice.call(document.querySelectorAll(l)).filter(function(e){return e===t});null!==l&&0<o.length&&(this._selector=l,this._triggerArray.push(a))}this._parent=this._config.parent?this._getParent():null,this._config.parent||this._addAriaAndCollapsedClass(this._element,this._triggerArray),this._config.toggle&&this.toggle()}var e,t,n,r=o.prototype;return r.toggle=function(){u(this._element).hasClass(d)?this.hide():this.show()},r.show=function(){var e,t,n=this;if(!this._isTransitioning&&!u(this._element).hasClass(d)&&(this._parent&&0===(e=[].slice.call(this._parent.querySelectorAll(C)).filter(function(e){return e.getAttribute("data-parent")===n._config.parent})).length&&(e=null),!(e&&(t=u(e).not(this._selector).data(c))&&t._isTransitioning))){var r=u.Event(h.SHOW);if(u(this._element).trigger(r),!r.isDefaultPrevented()){e&&(o._jQueryInterface.call(u(e).not(this._selector),"hide"),t||u(e).data(c,null));var i=this._getDimension();u(this._element).removeClass(m).addClass(p),this._element.style[i]=0,this._triggerArray.length&&u(this._triggerArray).removeClass(_).attr("aria-expanded",!0),this.setTransitioning(!0);var a="scroll"+(i[0].toUpperCase()+i.slice(1)),l=s.getTransitionDurationFromElement(this._element);u(this._element).one(s.TRANSITION_END,function(){u(n._element).removeClass(p).addClass(m).addClass(d),n._element.style[i]="",n.setTransitioning(!1),u(n._element).trigger(h.SHOWN)}).emulateTransitionEnd(l),this._element.style[i]=this._element[a]+"px"}}},r.hide=function(){var e=this;if(!this._isTransitioning&&u(this._element).hasClass(d)){var t=u.Event(h.HIDE);if(u(this._element).trigger(t),!t.isDefaultPrevented()){var n=this._getDimension();this._element.style[n]=this._element.getBoundingClientRect()[n]+"px",s.reflow(this._element),u(this._element).addClass(p).removeClass(m).removeClass(d);var r=this._triggerArray.length;if(0<r)for(var i=0;i<r;i++){var a=this._triggerArray[i],l=s.getSelectorFromElement(a);if(null!==l)u([].slice.call(document.querySelectorAll(l))).hasClass(d)||u(a).addClass(_).attr("aria-expanded",!1)}this.setTransitioning(!0);this._element.style[n]="";var o=s.getTransitionDurationFromElement(this._element);u(this._element).one(s.TRANSITION_END,function(){e.setTransitioning(!1),u(e._element).removeClass(p).addClass(m).trigger(h.HIDDEN)}).emulateTransitionEnd(o)}}},r.setTransitioning=function(e){this._isTransitioning=e},r.dispose=function(){u.removeData(this._element,c),this._config=null,this._parent=null,this._element=null,this._triggerArray=null,this._isTransitioning=null},r._getConfig=function(e){return(e=a({},g,e)).toggle=Boolean(e.toggle),s.typeCheckConfig(l,e,f),e},r._getDimension=function(){return u(this._element).hasClass(y)?y:v},r._getParent=function(){var n=this,e=null;s.isElement(this._config.parent)?(e=this._config.parent,void 0!==this._config.parent.jquery&&(e=this._config.parent[0])):e=document.querySelector(this._config.parent);var t='[data-toggle="collapse"][data-parent="'+this._config.parent+'"]',r=[].slice.call(e.querySelectorAll(t));return u(r).each(function(e,t){n._addAriaAndCollapsedClass(o._getTargetFromElement(t),[t])}),e},r._addAriaAndCollapsedClass=function(e,t){if(e){var n=u(e).hasClass(d);t.length&&u(t).toggleClass(_,!n).attr("aria-expanded",n)}},o._getTargetFromElement=function(e){var t=s.getSelectorFromElement(e);return t?document.querySelector(t):null},o._jQueryInterface=function(r){return this.each(function(){var e=u(this),t=e.data(c),n=a({},g,e.data(),"object"==typeof r&&r?r:{});if(!t&&n.toggle&&/show|hide/.test(r)&&(n.toggle=!1),t||(t=new o(this,n),e.data(c,t)),"string"==typeof r){if(void 0===t[r])throw new TypeError('No method named "'+r+'"');t[r]()}})},e=o,n=[{key:"VERSION",get:function(){return"4.1.3"}},{key:"Default",get:function(){return g}}],(t=null)&&i(e.prototype,t),n&&i(e,n),o}(),u(document).on(h.CLICK_DATA_API,T,function(e){"A"===e.currentTarget.tagName&&e.preventDefault();var n=u(this),t=s.getSelectorFromElement(this),r=[].slice.call(document.querySelectorAll(t));u(r).each(function(){var e=u(this),t=e.data(c)?"toggle":n.data();o._jQueryInterface.call(e,t)})}),u.fn[l]=o._jQueryInterface,u.fn[l].Constructor=o,u.fn[l].noConflict=function(){return u.fn[l]=n,o._jQueryInterface},o});
// Collapse

const textFields = document.querySelectorAll( '.mdc-text-field' );
for ( const textField of textFields ) {
  mdc.textField.MDCTextField.attachTo( textField );
}

const buttonRipples = document.querySelectorAll( '.mdc-button' );
for ( const buttonRipple of buttonRipples ) {
  mdc.ripple.MDCRipple.attachTo( buttonRipple );
}

const inputSelects = document.querySelectorAll( '.mdc-select' );
for ( const select of inputSelects ) {
  mdc.select.MDCSelect.attachTo( select );
}
const menuDropdowns = document.querySelectorAll( '.mdc-menu' );
for ( const menu of menuDropdowns ) {
  mdc.menu.MDCMenu.attachTo( menu );
}
const menuEls = Array.from( document.querySelectorAll( '.mdc-menu.menu-dropdown' ) );

const dialogs = document.querySelectorAll( '.mdc-dialog' );
for ( const dialog of dialogs ) {
  mdc.dialog.MDCDialog.attachTo( dialog );
}

menuEls.forEach( ( menuEl ) => {
  const menu = new mdc.menu.MDCMenu( menuEl );

  const dropdownToggle = menuEl.parentElement.querySelector( '.menu-dropdown-button' );
  dropdownToggle.addEventListener( 'click', () => {
    menu.open = ! menu.open;
  } );
} );
// const radioEls = document.querySelectorAll( '.mdc-radio__native-control' );
// for ( const radio of radioEls ) {
//   radio.addEventListener( 'change', () => {
//     if ( radio.checked ) {
//       radio.parentNode.parentNode.classList.add( 'checked' );
//     } else {
//       radio.parentNode.parentNode.classList.remove( 'checked' );
//     }
//   } );
// }

$( document ).ready( () => {
  // $('.scrollbar-macosx').scrollbar({
  //   "scrollx": null,
  //   "scrolly": null
  // });
  $( document )
    .on( 'click', '.btn-toggle-menu', function( event ) {
      // Open Menu
      event.preventDefault();
      $( 'body' ).addClass( 'open-sidebar' );
      $( this ).addClass( 'btn-menu-actived' );
    } )
    .on( 'click', '.btn-toggle-menu.btn-menu-actived, .open-sidebar .main-body', function( event ) {
      // Close Menu
      event.preventDefault();
      $( 'body' ).removeClass( 'open-sidebar' );
      $( '.btn-menu-actived' ).removeClass( 'btn-menu-actived' );
    } )
    .on( 'click', '.btn-modal', function( event ) {
      event.preventDefault();
      const targetModal = $( this ).data( 'target' );
      $( 'body' ).addClass( 'mdc-dialog-scroll-lock' );
      $( document ).find( targetModal ).addClass( 'mdc-dialog--open' );
    } )
    .on( 'click', '.btn-expand-all', function( event ) {
      event.preventDefault();
      if ( $(this).hasClass('collapsed') ) {
        $(this).removeClass('collapsed');
        $(this).parents('.mdc-card').find('.expand-all').collapse('show');
      } else if ( !$(this).hasClass('collapsed') ){
        $(this).addClass('collapsed');
        $(this).parents('.mdc-card').find('.expand-all').collapse('hide');
      }
      } )
      .on( 'click', '.mdc-tab', function( event ) {
        event.preventDefault();
        const $parents = $(this).parents('.mdc-tab-bar');
        $(this).addClass('mdc-tab--active').siblings('.mdc-tab').removeClass('mdc-tab--active');
        $parents.find($(this).data('tab')).removeClass('d-none').siblings('.tab-content').addClass('d-none');
      });
    // .on( 'click', '.mdc-dialog-scroll-lock', function() {
    //   closeModal();
    //   $( document ).find( '.mdc-dialog--open' ).removeClass( 'mdc-dialog--open' );
    // } );
  $( '.mdc-radio__native-control' ).change( function() {
    $( this ).parents( '.group-radio' ).find( '.mdc-form-field' ).removeClass( 'checked' );
    $( this ).parents( '.mdc-form-field' ).addClass( 'checked' );
  } );
  $( '.select-all-checkbox' ).change( function() {
    $(document).find('.select-all-show').toggleClass('d-none');
  });

  
} );

var closeModal = function() {
  $( 'body' ).removeClass( 'mdc-dialog-scroll-lock' );
};
